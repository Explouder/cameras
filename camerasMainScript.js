$(document).ready(function()
{
  //Размещается ли камера пользователем
  var placingCamera = false;
  //Вращается ли камера пользователем
  var rotatingCamera = false;

  //Номер выбранной комнаты
  var currentFloor;
  //Изображение выбранной комнаты
  var currentFloorImg;

  //Изображение камеры
  var cameraImageSrc = "img/cameraIcon.png";
  var cameraImageSize = 32;
  var placingCamera_x, placingCamera_y;
  var cameraBrightness = 0;
  var currentCameraElement;
  //id последней камеры на которую был наведен курсор
  var cameraUnderCursorID = -1;
  var areaUnderCursorID = -1;
  //var lastCameraChanged = false;
  //Массив камер как элементов jQuery
  var camQuery = [];
  //Массив разметок как элементов jQuery
  var areaQuery = [];
  //Размеры карты, мини-карт
  var mapSize = parseFloat($("#map").css("width"));
  var miniMapSize = 128;
  //Количество отображаемых карт для выбора
  //(отрисовка будет для miniMapNumber+1, т.к. предполагается, что одна карта будет скрыта)
  var miniMapNumber = 3;
  //Изображения комнат
  var mapsImg=[];
  //Изображение кнопки разметки комнат
  var areaButtonImageSrc = "img/areaButton.jpg";
  var placingArea = false;
  var extendingArea = false;
  var areaImageSrc = "img/areaImage.png";
  var areaImageSize = 32;
  var cursorImageSrc = cameraImageSrc;
  //Блоки кода камер как элементов html
  var camerasCodeBlock = [];
  //Блоки кода карт комнат как элементов html
  var maps = [];
  //Информация о позиции камер и их номер
  var camerasPosition = [];
  //Информация о разметках
  var areas = [];
  //Максимум точек в одной области
  var areaMaxDots = 64;
  //Максимальная длина названия комнаты
  var areaNameMaxLength = 32;
  //Текущая разметка (добавляемая пользователем в режиме разметки, но не сохраненная в файл)
  var areasTemp = [];
  //Текущие точки режима разметки
  var dotsTemp = [];
  //Блоки кода разметок
  var areasCodeBlock = [];
  //Блоки кода названий комнат (разметок)
  var areasNamesCodeBlock = [];
  var areasNamesCoordsX = [];
  var areasNamesCoordsY = [];
  var tempAreaCodeBlock;
  //Расстояние в пикселах от клика до первой размещенной точки текущей разметки при котором разметка прекращается.
  var areaLastDotRange = 10;
  //Загрузить изображения карт
  function loadMapsImages() {
  }
  //Хард код загрузки изображений карт
  mapsImg.push("img/map0.jpg");
  mapsImg.push("img/map1.jpg");
  mapsImg.push("img/map1.jpg");
  mapsImg.push("img/map1.jpg");
  mapsImg.push("img/map1.jpg");
  mapsImg.push("img/map1.jpg");
  mapsImg.push("img/map1.jpg");
  mapsImg.push("img/map1.jpg");
  mapsImg.push("img/map1.jpg");
  mapsImg.push("img/map0.jpg");

  
  for (let i = 0; i < mapsImg.length; i++) 
  {
    maps.push(makeImageCodeBlock(mapsImg[i], miniMapSize, miniMapSize, "curtainFloor", "m" + i));
  }

  //Вставка css кода в head
  function insertCss(code) 
  {
    var style = document.createElement('style');
    style.type = 'text/css';

    if (style.styleSheet) 
    {// IE
      style.styleSheet.cssText = code;
    }
    else 
    {// Другие браузеры
      style.innerHTML = code;
    }
    document.getElementsByTagName("head")[0].appendChild(style);
  }

  //Загрузить позиции камер из БД
  function loadCamerasPosition() {
  }

  //Сохранить позиции камер в БД
  function saveCamerasPosition() {
  }

  //Пока БД нет, заполняем хард кодом
  camerasPosition.push(
    {
      x: 100,
      y: 100,
      floor: 0,
      numb: 0,
      angle: 90
    });//Одна камера в комнате 0

  camerasCodeBlock.push('<img src =' + cameraImageSrc + ' class="cameraElement" id="c' + camerasCodeBlock.length + '">');
  for(let i=0; i<camerasCodeBlock.length; i++)
  {
    insertCss(
      "#c" + i + " \
      {position: absolute;\
      left: "+ camerasPosition[i].x + "px;\
      top: "+ camerasPosition[i].y + "px;\
      z-index: 100;\
      transform: rotate("+ camerasPosition[i].angle + "deg);\
      }"
    );
  }

  //Загрузить разметку комнат
  function loadAreas() {
  }

  //Пока БД нет, заполняем хард кодом
  areas.push(
    {
      name: "unnamed area",
      floor: 0,
      dots: [
              {x: 20, y: 20},
              {x: 100, y: 20},
              {x: 100, y: 100},
              {x: 20, y: 100}
            ]
    });//Одна разметка в комнате 0
    
  //Заполняет массив блоков кода разметок и их названий исходя из массива areas
  var makeAreasCodeBlock = function()
  {
    areasCodeBlock.length = 0;
    areasNamesCodeBlock.length = 0;

    for(let i = 0;i < areas.length;i++)
    {
      var mostLeftDotX = -1;
      var mostRightDotX = Infinity;
      var mostTopDotY = -1;
      var mostBotDotY = Infinity;
      var centerX;
      var centerY;
      var points="";
      for(let j = 0;j < areas[i].dots.length;j++)
      {
        //Расчет позиции текста
        if(areas[i].dots[j].x > mostLeftDotX)
          mostLeftDotX = areas[i].dots[j].x;
        if(areas[i].dots[j].x < mostRightDotX)
          mostRightDotX = areas[i].dots[j].x;
        if(areas[i].dots[j].y > mostTopDotY)
          mostTopDotY = areas[i].dots[j].y;
        if(areas[i].dots[j].y < mostBotDotY)
          mostBotDotY = areas[i].dots[j].y;

        points += areas[i].dots[j].x;
        points += ",";
        points += areas[i].dots[j].y;
        if(j!=areas[i].dots.length - 1)
          points += " ";
      }
      centerX = (mostLeftDotX+mostRightDotX)/2;
      centerY = (mostTopDotY+mostBotDotY)/2;
      areasNamesCoordsX[i] = centerX;
      areasNamesCoordsY[i] = centerY;

      areasCodeBlock.push('<polygon id = "poly'+i+'" points="'+points+'" fill="orange" class = areaElement></polygon>');
      areasNamesCodeBlock.push('<p>'+areas[i].name+'</p>');
      //console.log('<polygon id = "poly'+i+'" points="'+points+'" fill="orange"></polygon>');
    }
  }
  makeAreasCodeBlock();

  //Отрисовка выбора комнаты
  for (let i = 0; i < maps.length; i++) 
  {
    $(maps[i]).appendTo("#floor");
  }

  //Удалить все дочерние элементы элемента с указанным id 
  function clearElement(elementId) 
  {
    var myNode = document.getElementById(elementId);
    while (myNode.firstChild) { myNode.removeChild(myNode.firstChild); }
  }
  //Сформировать блок кода изображения
  function makeImageCodeBlock(imgSrc, imgWidth, imgHeight, imgClass, imgId)
  {
    return ('<img src = "'+imgSrc+'" width="'+imgWidth+'" height="'+imgHeight+'" class="'+imgClass+'" id="'+imgId+'">');
  }

  //Отрисовка камер заданной комнаты
  var drawCameras = function (floor) 
  {
    for (let i = 0; i < camerasPosition.length; i++) 
    {
      if (floor == camerasPosition[i].floor) 
      {
        //Добавляем html блок
        camQuery[i]=$(camerasCodeBlock[i]).appendTo("#camerasMap");
        console.log("init "+$(camQuery[i]))
        //Функционал камеры
        $(camQuery[i]).click(function () 
        { 
          if(!placingCamera)
          {
            console.log('click');
          }
        });
        $(camQuery[i]).mouseenter(function () 
        {
          console.log("enetered");
          
        });
        $(camQuery[i]).mouseleave(function () 
        { 
          
        });
        $("#c"+i).css({"position": "absolute", "left": ""+camerasPosition[i].x+"px", "top": ""+camerasPosition[i].y+"px", "z-index" : 100, "transform" : "rotate("+camerasPosition[i].angle+" deg)"});
      }
    }
  }

  var drawAreas = function(floor)
  {
    for(let i = 0;i < areas.length; i++)
    {
      if (floor == areas[i].floor)
      {
        $(areasCodeBlock[i]).appendTo("#svgOutline");
      }
    }
    $("#svgOutline").html($("#svgOutline").html());
  }

  var drawAreasNames = function(floor)
  {
    for(let i = 0;i < areas.length; i++)
    {
      if (floor == areas[i].floor)
      {
        var areaName = [];
        var areaNameBorder;

        areaNameBorder = $("<div style='position: absolute; text-align: center; width: auto; height: auto; white-space: nowrap;'></div>").appendTo("#areasNames");
        
        areaName[i] = $(areasNamesCodeBlock[i]).appendTo(areaNameBorder);
        areaNameBorder.html(areaNameBorder.html());
        //console.log(areasNamesCoordsX[i]+" arcX");
        //console.log((parseFloat($(areaNameBorder).css("height")))/2+" arcX");
        areaNameBorder.css("top", areasNamesCoordsY[i] - (parseFloat($(areaNameBorder).css("height")))/2);
        areaNameBorder.css("left", areasNamesCoordsX[i] - (parseFloat($(areaNameBorder).css("width")))/2);
        //console.log(parseFloat($(areaNameBorder).css("width")));
       // $(areaNameBorder).css({"position":"absolute","left":(areasNamesCoordsX[i] - $(areaNameBorder).css("width")/2)+"px", "top":(areasNamesCoordsY[i] - $(areaNameBorder).css("height")/2)+"px", "text-align":"center"});
        //areaNameBorder.html(areaNameBorder.html());
      }
    }
    $("#areasNames").html($("#areasNames").html());
  }

  //Нажатие на полигон
  $('body').on("click",".areaElement", function(e)
  {
    //Читам id с 4 символа (id вида: "poly1")
    console.log("hi area"+this.id.substring(4, ));
  });

  //Наведение на полигон
  $('body').on("mouseenter",".areaElement", function(e)
  {
    $(this).css({"fill":"orangered"});
    //Читам id с 4 символа (id вида: "poly1")
    console.log("enter area"+this.id.substring(4, ));
  });

  //Покидание курсором полигона
  $('body').on("mouseleave",".areaElement", function(e)
  {
    $(this).css({"fill":"orange"});
    //Читам id с 4 символа (id вида: "poly1")
    console.log("leave area"+this.id.substring(4, ));
  });


  //Нажатие на камеру
  $('body').on("click",".cameraElement", function(e)
  {
    //Читам id с 4 символа (id вида: "c1")
    console.log("hi cam"+this.id.substring(1, ));
  });

  //Наведение на камеру
  $('body').on("mouseenter",".cameraElement", function(e)
  {
    if(!placingCamera && !placingArea)
    {
      cameraUnderCursorID = this.id;
      $(this).addClass('mouseEnter');
      setTimeout((function()
      {
        $(this).removeClass('mouseEnter');
          if(cameraUnderCursorID == this.id)
          {
            $(this).css({"filter":"brightness(8)"});
          }
      }).bind(this)
      , 900);
    }
    //Читам id с 4 символа (id вида: "c1")
    console.log("enter cam"+this.id.substring(1, ));
  });

  //Покидание курсором камеры
  $('body').on("mouseleave",".cameraElement", function(e)
  {
    if(!placingCamera)
          {
            $(this).css({"filter":"brightness(1)"});
            $(this).removeClass('mouseEnter');
            cameraUnderCursorID = null;
          }
    //Читам id с 4 символа (id вида: "c1")
    console.log("leave cam"+this.id.substring(1, ));
  });

  //Рисует полигон из массива dotsTemp
  //Параметры x, y - координаты курсора
  var drawTempArea = function(x, y)
  {
    var points="";
    tempAreaCodeBlock = '';
    for(let i = 0;i < dotsTemp.length;i++)
    {
      points += dotsTemp[i].x;
      points += ",";
      points += dotsTemp[i].y;
      points += " ";
    }
    tempAreaCodeBlock = '<polygon id = "poly'+'Temp'+'" points="'+points+x+','+y+'" fill="orange"></polygon>';
    //console.log(tempAreaCodeBlock);
    areaQuery[currentFloor]=$(tempAreaCodeBlock).appendTo("#svgOutline");
    if(dotsTemp.length > 0)
    {
      $("<svg id = 'firstDotShell'></svg>").appendTo("#svgOutline");
      $("<circle id = 'firstDot' cx='"+dotsTemp[0].x+"' cy='"+dotsTemp[0].y+"' r='10'/>").appendTo("#firstDotShell");
    }
    $("#svgOutline").html($("#svgOutline").html());
  }

//Добавление камеры кликом
  document.getElementById('camerasMap').addEventListener('click', function (e) 
  {
    var pgX = e.pageX;
    var pgY = e.pageY;
    var box = this.getBoundingClientRect();

    if (placingCamera) 
    {
      if (rotatingCamera) 
      {
        var ang = document.getElementById('mouseCamera').style.transform;
        ang = ang.split('d')[0];
        ang = ang.split('(')[1];
        var newCameraCodeBlock = makeImageCodeBlock(cameraImageSrc, cameraImageSize, cameraImageSize, "cameraElement", "c" + camerasCodeBlock.length);
        camerasCodeBlock.push(newCameraCodeBlock);
        camerasPosition.push(
        {
          x: placingCamera_x - box.left - cameraImageSize/2,
          y: placingCamera_y - box.top - cameraImageSize/2,
          floor: currentFloor,
          numb: camerasPosition.length,
          angle: parseFloat(ang)
        });
        rotatingCamera = false;
        insertCss(
          "#c" + (camerasPosition.length-1).toString() + " \
          {position: absolute;\
          left: "+ camerasPosition[camerasPosition.length-1].x + "px;\
          top: "+ camerasPosition[camerasPosition.length-1].y + "px;\
          z-index: 100;\
          transform: rotate("+ camerasPosition[camerasPosition.length-1].angle + "deg);\
          }"
        );
        drawFloor(currentFloor);
      }
      else 
      {
        placingCamera_x = pgX;
        placingCamera_y = pgY; 
        rotatingCamera = true;
      }
    }
  }, false);

  //Добавление разметки кликом
  document.getElementById('svgAreas').addEventListener('click', function (e) 
  {
    var pgX = e.pageX;
    var pgY = e.pageY;
    var box = this.getBoundingClientRect();
    if (placingArea) 
    {
      var placingAreaX =  pgX - box.left;
      var placingAreaY =  pgY - box.top;
      if (extendingArea) 
      {
        if(Math.abs(dotsTemp[0].x - placingAreaX) < areaLastDotRange &&
           Math.abs(dotsTemp[0].y - placingAreaY) < areaLastDotRange &&
           dotsTemp.length > 2)
        {
          //Закрытие разметки (когда пользователь кликает на первую точку)
          extendingArea = false;
          placingArea = false;
          var newAreaName = prompt("enter area name");
          if(newAreaName == "" || typeof(newAreaName) != "string" || newAreaName.length > areaNameMaxLength)
            {newAreaName = "unnamed area"+areas.length;}
          
            areas.push(
            {
              name: newAreaName,
              floor: currentFloor,
              dots: []
            });
            for (let i = 0; i < dotsTemp.length; i++)
              areas[areas.length-1].dots.push(dotsTemp[i]);
            clearElement("svgOutline");
            clearElement("areasNames");
            drawTempArea(dotsTemp[0].x, dotsTemp[0].y);
            dotsTemp.length = 0;
            makeAreasCodeBlock();
            drawAreas(currentFloor);
            drawAreasNames(currentFloor);
            clearElement("firstDotShell");
            console.log("made area");
        }
        else
        {
          //Точки после первой
          if(dotsTemp.length < areaMaxDots)
           {
            dotsTemp.push({x: placingAreaX, y: placingAreaY});
            console.log("extended area");
           }
        }
      }
      else 
      {
        //Первая точка
        dotsTemp.push({x: placingAreaX, y: placingAreaY});
        extendingArea = true;
        console.log("started making area");
      }
    }
  }, false);

  //Отрисовка карты и всех эл-в
  function drawFloor(floor)
  {
    clearElement("map");
    clearElement("camerasMap");
    clearElement("areasNames");
    currentFloorImg.appendTo("#map");
    drawCameras(floor);
    drawAreas(floor);
    drawAreasNames(floor);
  }

  //Когда область мини-карт загрузилась
  $( "#floor" ).ready(function() 
    {
      //Выбор комнаты
      $(".curtainFloor").click(function() 
      {
        clearElement("svgOutline");
        placingCamera = false;
        rotatingCamera = false;
        currentFloor = +(this.id).substring(1);
        currentFloorImg = $(makeImageCodeBlock(this.src, mapSize, mapSize,"","mapImg"));
        drawFloor(currentFloor);
        //Можно вынести отрисовку загруженных областей в отдельный блок, чтобы не перерисовывать каждый раз статичные картинки
        //drawAreas(currentFloor);
      });
    });
    
    //Активация режима размещения камер
    $( "#placeCameraButton" ).ready(function() 
    {
      $( "#placeCameraButton" ).click(function() 
      {
        if(currentFloor != undefined)
        {
          if(!placingCamera)
          {
            $("#camerasMap").css({"pointer-events": "auto"});
            cursorImageSrc = cameraImageSrc;
            document.onmousemove = mouseMove;
            placingCamera = true;
          }
          else
          {
            $("#camerasMap").css({"pointer-events": "none"});
            rotatingCamera = false;
            placingCamera = false; 
          }
        }
      });
    });
    
    //Активация режима разметки
    $( "#placeAreaButton" ).ready(function() 
    {
      $( "#placeAreaButton" ).click(function() 
      {
        if(currentFloor != undefined)
        {
          if(!placingArea)
          {
            $("#svgAreas").css({"pointer-events": "auto"});
            placingArea = true;
            cursorImageSrc = areaImageSrc;
            document.onmousemove = mouseMove;
          }
          else
          {
            $("#svgAreas").css({"pointer-events": "none"});
            extendingArea = false;
            placingArea = false;
          }
        }
      });
    });

    //Отрисовка иконки следующей за курсором и разметки новой области
    var mouseMove = function ()
    {
      document.getElementById("cursor").innerHTML = '<img id="mouseCamera" src="'+cursorImageSrc+'" style="position: fixed; pointer-events: none; display: none; left: 0px; top: 0px;">';
      
      if(document.getElementById("mouseCamera") != null)
      {
        if(placingArea)
          cursorImageSrc = areaImageSrc;
        var mouseCamera = document.getElementById("cursor").firstChild;
        mouseCamera.src = cursorImageSrc;
        console.log("a");
        mouseCamera.style.position = 'fixed';
        mouseCamera.style.pointerEvents = "none";
        document.onmousemove = function(event)
        {
          var mapBox = document.getElementById("map").getBoundingClientRect();
          if((event.clientY - cameraImageSize/2 > mapBox.top) &&
             (event.clientX - cameraImageSize/2 > mapBox.left) &&
             (event.clientY - cameraImageSize/2 < mapBox.top + mapSize) &&
             (event.clientX - cameraImageSize/2 < mapBox.left + mapSize) &&
             (placingCamera || placingArea))
          {
            mouseCamera.style.display = "block";
            if(rotatingCamera)
            {
              mouseCamera.style.left = placingCamera_x - cameraImageSize/2+'px';
              mouseCamera.style.top = placingCamera_y - cameraImageSize/2+'px';
              var x1 = placingCamera_x - cameraImageSize/2,
                  y1 = placingCamera_y - cameraImageSize/2;
              var x2 = event.clientX - cameraImageSize/2,
                  y2 = event.clientY - cameraImageSize/2;
              var ang = Math.atan2(y1 - y2, x1 - x2) / Math.PI * 180;
              ang = (ang < 0) ? ang + 360 : ang;
              mouseCamera.style.transform = 'rotate(' + ang + 'deg)';
            }
            else
            {
              mouseCamera.style.left = event.clientX - cameraImageSize/2+'px';
              mouseCamera.style.top = event.clientY - cameraImageSize/2+'px';
            }
          }
          else
          {
            mouseCamera.style.display = "none";
          }
        }
        //Новая разметка
        document.getElementById("svgAreas").onmousemove = function(event)
        {
          var mapBox = document.getElementById("svgAreas").getBoundingClientRect();
          if((event.clientY - areaImageSize/2 > mapBox.top) &&
             (event.clientX - areaImageSize/2 > mapBox.left) &&
             (event.clientY - areaImageSize/2 < mapBox.top + mapSize) &&
             (event.clientX - areaImageSize/2 < mapBox.left + mapSize) &&
             (placingArea || extendingArea))
             {
              clearElement("svgOutline");
              drawTempArea(event.clientX - mapBox.left, event.clientY - mapBox.top);
              //Рисуем на движение мыши, можно сделать одну загрузку, но пока так т.к. возможно понадобится анимация
              drawAreas(currentFloor);
             }
        }
      }
    }
    document.onmousemove = mouseMove;

    //Отрисовка мини-карт в зависимости от положения ползунка
    function refreshMaps( event, ui ) 
    {
      var val = $("#slider").slider( "option", "value" );
      for(let i=0;i<=mapsImg.length;i++)
      {
        $("#m"+i).css({"position": "absolute","top":val+miniMapSize*i-miniMapSize*miniMapNumber});
        if(val+miniMapSize*i-512 > miniMapSize*(miniMapNumber-1) || val+miniMapSize*i-512 < -miniMapSize*2)
        {
          $("#m"+i).css({"display": "none"});
        }
        else
        {
          $("#m"+i).css({"display": "block"});
        }
      } 
    };

    //Ползунок
    $("#slider").slider(
    {
      min: -miniMapSize*(miniMapNumber+1),
      max: miniMapSize*miniMapNumber,
      range: false,
      orientation: "vertical",
      value: miniMapSize*miniMapNumber,
      slide: refreshMaps,
      create: refreshMaps
    });
});
